@extends('layouts.app')
@section('content')
        <div class="row align-items-center">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Email</th>
                    <th scope="col">username</th>
                    <th scope="col">Quotes count</th>
                    <th scope="col">Show</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Remove</th>
                </tr>
                </thead>
                <tbody>
                @foreach($authors as $author)
                    <tr>
                        <th scope="row">{{ $author->id }}</th>
                        <td>{{ $author->email }}</td>
                        <td>{{ $author->username }}</td>
                        <td>{{ $author->quotes->count() }}</td>
                        <td><a class="btn btn-success" href="{{ route('author.show', $author->id) }}">Show</a></td>
                        <td><a class="btn btn-primary" href="{{ route('author.edit', $author->id) }}">Edit</a></td>
                        <td>
                            <form action="{{ route('author.destroy', $author->id) }}" method="post">
                                @csrf
                                @method('DELETE')

                                <button class="btn btn-danger" type="submit">Remove</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $authors->links() }}

            <a class="btn btn-success" href="{{ route('author.create') }}" style="width: 150px">Create</a>
        </div>
@endsection
