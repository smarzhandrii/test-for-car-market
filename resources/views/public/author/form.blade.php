<form method="POST" action="{{ route($route, $author->id) }}">
    @csrf

    @if($route == 'author.update')
        @method('PATCH')
    @endif

    <div class="row mb-3">
        <label for="username" class="col-md-4 col-form-label text-md-end">Username</label>

        <div class="col-md-6">
            <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ $author->username ?? old('username') }}" required autocomplete="username" autofocus>

            @error('username')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    @if($route == 'author.update')
        <div class="row mb-3 align-items-center">
            <p class="h4 text-center">Email:  {{ $author->email}}</p>
        </div>
    @else
        <div class="row mb-3">
            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $author->email ?? old('email') }}" required autocomplete="email">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    @endif

    <div class="row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                Отправить
            </button>
        </div>
    </div>
</form>
