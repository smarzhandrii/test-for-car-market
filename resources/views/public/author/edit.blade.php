@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Author</div>
                    <div class="card-body">
                        @include('public.author.form' , [
                            'author' => $author,
                            'route' => 'author.update'
                        ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
