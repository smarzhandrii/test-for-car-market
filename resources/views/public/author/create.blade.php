@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create Author</div>
                    <div class="card-body">
                        @include('public.author.form' , [
                            'author' => $author,
                            'route' => 'author.store'
                        ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
