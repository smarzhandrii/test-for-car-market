@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Author #{{ $author->id }}</div>
                    <div class="card-body">
                        <dl class="row">
                            <dt class="col-sm-3">ID</dt>
                            <dd class="col-sm-9">{{ $author->id }}</dd>

                            <dt class="col-sm-3">Author Email</dt>
                            <dd class="col-sm-9">{{ $author->email }}</dd>

                            <dt class="col-sm-3">Author username</dt>
                            <dd class="col-sm-9">{{ $author->username }}</dd>

                            <dt class="col-sm-3">Quotes count</dt>
                            <dd class="col-sm-9">{{ $author->quotes->count() }}</dd>

                            <dt class="col-sm-3">Edit</dt>
                            <dd class="col-sm-9"><a class="btn btn-primary" href="{{ route('author.edit', $author->id) }}">Edit</a></dd>

                            <dt class="col-sm-3">Remove</dt>
                            <dd class="col-sm-9">
                                <form action="{{ route('author.destroy', $author->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')

                                    <button class="btn btn-danger" type="submit">Remove</button>
                                </form>
                            </dd>
                        </dl>

                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Author Email</th>
                                <th scope="col">Author username</th>
                                <th scope="col">Content</th>
                                <th scope="col">Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($author->quotes as $quote)
                                <tr>
                                    <th scope="row">{{ $quote->id }}</th>
                                    <td>{{ $author->email }}</td>
                                    <td>{{ $author->username }}</td>
                                    <td>{{ $quote->content }}</td>
                                    <td><a class="btn btn-primary" href="{{ route('quote.edit', $quote->id) }}">Edit</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
