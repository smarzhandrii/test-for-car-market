@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Quote #{{ $quote->id }}</div>
                    <div class="card-body">
                        <dl class="row">
                            <dt class="col-sm-3">ID</dt>
                            <dd class="col-sm-9">{{ $quote->id }}</dd>

                            <dt class="col-sm-3">Author Email</dt>
                            <dd class="col-sm-9">
                                <a class="btn btn-primary" href="{{ route('author.show', $quote->author->id) }}">
                                    {{ $quote->author->email }}
                                </a>
                            </dd>

                            <dt class="col-sm-3">Author username</dt>
                            <dd class="col-sm-9">{{ $quote->author->username }}</dd>

                            <dt class="col-sm-3">Content</dt>
                            <dd class="col-sm-9">{{ $quote->content }}</dd>

                            <dt class="col-sm-3">Edit</dt>
                            <dd class="col-sm-9"><a class="btn btn-primary" href="{{ route('quote.edit', $quote->id) }}">Edit</a></dd>

                            <dt class="col-sm-3">Remove</dt>
                            <dd class="col-sm-9">
                                <form action="{{ route('quote.destroy', $quote->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')

                                    <button class="btn btn-danger" type="submit">Remove</button>
                                </form>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-5">
            <div class="col-md-8">
                <h3>Quote Sharing</h3>
                <div class="card justify-content-center mt-5">
                    <div class="card-header">Telegram</div>
                    <div class="card-body">
                        <form action="{{ route('quote.sharing.telegram', $quote->id) }}" method="post">
                            @csrf

                            <div class="row mb-3">
                                <label for="telegram" class="col-md-4 col-form-label text-md-end">Telegram</label>

                                <div class="col-md-6">
                                    <input id="telegram" type="text" class="form-control @error('telegram') is-invalid @enderror" name="telegram" value="" required autocomplete="telegram" autofocus>

                                    @error('telegram')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>

                            <button class="btn btn-danger" type="submit">Share</button>
                        </form>
                    </div>
                </div>
                <div class="card justify-content-center mt-5">
                    <div class="card-header">Email</div>
                    <div class="card-body">
                        <form action="{{ route('quote.sharing.email', $quote->id) }}" method="post">
                            @csrf

                            <div class="row mb-3">
                                <label for="email" class="col-md-4 col-form-label text-md-end">Email</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>

                            <button class="btn btn-danger" type="submit">Share</button>
                        </form>
                    </div>
                </div>
                <div class="card justify-content-center mt-5">
                    <div class="card-header">Viber</div>
                    <div class="card-body">
                        <form action="{{ route('quote.sharing.viber', $quote->id) }}" method="post">
                            @csrf

                            <div class="row mb-3">
                                <label for="viber" class="col-md-4 col-form-label text-md-end">Viber</label>

                                <div class="col-md-6">
                                    <input id="viber" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="" required autocomplete="viber" autofocus>

                                    @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <button class="btn btn-danger" type="submit">Share</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
