@extends('layouts.app')
@section('content')
        <div class="row align-items-center">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Author Email</th>
                    <th scope="col">Author username</th>
                    <th scope="col">Content</th>
                    <th scope="col">Show</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Remove</th>
                </tr>
                </thead>
                <tbody>
                @foreach($quotes as $quote)
                    <tr>
                        <th scope="row">{{ $quote->id }}</th>
                        <td>{{ $quote->author->email }}</td>
                        <td>{{ $quote->author->username }}</td>
                        <td>{{ $quote->content }}</td>
                        <td><a class="btn btn-success" href="{{ route('quote.show', $quote->id) }}">Show</a></td>
                        <td><a class="btn btn-primary" href="{{ route('quote.edit', $quote->id) }}">Edit</a></td>
                        <td>
                            <form action="{{ route('quote.destroy', $quote->id) }}" method="post">
                                @csrf
                                @method('DELETE')

                                <button class="btn btn-danger" type="submit">Remove</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $quotes->links() }}

            <a class="btn btn-success" href="{{ route('quote.create') }}" style="width: 150px">Create</a>
        </div>
@endsection
