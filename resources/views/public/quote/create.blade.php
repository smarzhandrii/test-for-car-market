@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create Quote</div>
                    <div class="card-body">
                        @include('public.quote.form' , [
                            'quote' => $quote,
                            'route' => 'quote.store'
                        ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
