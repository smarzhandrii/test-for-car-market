<?php

namespace App\Jobs\Quote\Sharing;

use App\Models\Quote;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TelegramSharingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $telegram;

    protected Quote $quote;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $telegram, Quote $quote)
    {
        $this->telegram = $telegram;
        $this->quote = $quote;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Send quote in telegram user
    }
}
