<?php

namespace App\Http\Controllers;

use App\Http\Requests\Author\AuthorCreateRequest;
use App\Http\Requests\Author\AuthorUpdateRequest;
use App\Models\Author;
use App\Services\Author\AuthorCreateService;
use App\Services\Author\AuthorService;
use App\Services\Author\AuthorUpdateService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class AuthorController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        $authors = AuthorService::make()->getListWithPagination(10);

        return view('public.author.index', compact('authors'));
    }

    /**
     * @param Author $author
     * @return Factory|View|Application
     */
    public function show(Author $author): Factory|View|Application
    {
        return view('public.author.show', compact('author'));

    }

    /**
     * @return Factory|View|Application
     */
    public function create(): Factory|View|Application
    {
        return view('public.author.create', ['author' => new Author()]);
    }

    /**
     * @param AuthorCreateRequest $request
     * @return Redirector|Application|RedirectResponse
     */
    public function store(AuthorCreateRequest $request): Redirector|Application|RedirectResponse
    {
        $author = AuthorCreateService::make()->create($request);

        return redirect(route('author.edit', $author->id))->with('success', 'Create success');
    }

    /**
     * @param Author $author
     * @param AuthorUpdateRequest $request
     * @return Redirector|Application|RedirectResponse
     */
    public function update(Author $author, AuthorUpdateRequest $request): Redirector|Application|RedirectResponse
    {
        AuthorUpdateService::make($author)->update($request);

        return redirect(route('author.edit', $author->id))->with('success', 'Update success');
    }

    /**
     * @param Author $author
     * @return Factory|View|Application
     */
    public function edit(Author $author): Factory|View|Application
    {
        return view('public.author.edit', compact('author'));
    }

    /**
     * @param Author $author
     * @return Application|RedirectResponse|Redirector
     */
    public function destroy(Author $author): Redirector|RedirectResponse|Application
    {
        $author->delete();

        return redirect(route('author.index'))->with('success', 'Remove success');
    }
}
