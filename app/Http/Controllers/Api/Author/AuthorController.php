<?php

namespace App\Http\Controllers\Api\Author;

use App\Http\Controllers\Api\BaseApiController;
use App\Http\Requests\Author\AuthorCreateRequest;
use App\Http\Requests\Author\AuthorUpdateRequest;
use App\Http\Resources\Author\AuthorResource;
use App\Models\Author;
use App\Services\Author\AuthorCreateService;
use App\Services\Author\AuthorService;
use App\Services\Author\AuthorUpdateService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthorController extends BaseApiController
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return AuthorResource::collection(AuthorService::make()->getListWithPagination(10));
    }

    /**
     * @param Author $author
     * @return AuthorResource
     */
    public function show(Author $author): AuthorResource
    {
        return AuthorResource::make($author);
    }

    /**
     * @return AuthorResource
     */
    public function create(): AuthorResource
    {
        return AuthorResource::make(new Author());
    }

    /**
     * @param AuthorCreateRequest $request
     * @return AuthorResource
     */
    public function store(AuthorCreateRequest $request): AuthorResource
    {
        return AuthorResource::make(AuthorCreateService::make()->create($request));
    }

    /**
     * @param Author $author
     * @param AuthorUpdateRequest $request
     * @return AuthorResource
     */
    public function update(Author $author, AuthorUpdateRequest $request): AuthorResource
    {
        return AuthorResource::make(AuthorUpdateService::make($author)->update($request));
    }

    /**
     * @param Author $author
     * @return AuthorResource
     */
    public function edit(Author $author): AuthorResource
    {
        return AuthorResource::make($author);
    }

    /**
     * @param Author $author
     * @return JsonResource
     */
    public function destroy(Author $author): JsonResource
    {
        $author->delete();

        return $this->responseSuccess();
    }
}
