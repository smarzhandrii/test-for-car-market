<?php
namespace App\Http\Controllers\Api\Quote;

use App\Http\Controllers\Api\BaseApiController;
use App\Http\Requests\Quote\Sharing\QuoteSharingEmailRequest;
use App\Http\Requests\Quote\Sharing\QuoteSharingTelegramRequest;
use App\Http\Requests\Quote\Sharing\QuoteSharingViberRequest;
use App\Jobs\Quote\Sharing\EmailSharingJob;
use App\Jobs\Quote\Sharing\TelegramSharingJob;
use App\Jobs\Quote\Sharing\ViberSharingJob;
use App\Models\Quote;
use Illuminate\Http\Resources\Json\JsonResource;

class QuoteSharingController extends BaseApiController
{
    /**
     * @param Quote $quote
     * @param QuoteSharingTelegramRequest $request
     * @return JsonResource
     */
    public function sendToTelegram(Quote $quote, QuoteSharingTelegramRequest $request): JsonResource
    {
        $this->dispatch(new TelegramSharingJob($request->validated()['telegram'], $quote));

        return $this->responseSuccess();
    }

    /**
     * @param Quote $quote
     * @param QuoteSharingEmailRequest $request
     * @return JsonResource
     */
    public function sendToEmail(Quote $quote, QuoteSharingEmailRequest $request): JsonResource
    {
        $this->dispatch(new EmailSharingJob($request->validated()['email'], $quote));

        return $this->responseSuccess();

    }

    /**
     * @param Quote $quote
     * @param QuoteSharingViberRequest $request
     * @return JsonResource
     */
    public function sendToViber(Quote $quote, QuoteSharingViberRequest $request): JsonResource
    {
        $this->dispatch(new ViberSharingJob($request->validated()['phone'], $quote));

        return $this->responseSuccess();
    }
}
