<?php

namespace App\Http\Controllers\Api\Quote;

use App\Http\Controllers\Api\BaseApiController;
use App\Http\Requests\Quote\QuoteCreateRequest;
use App\Http\Requests\Quote\QuoteUpdateRequest;
use App\Http\Resources\Quote\QuoteResource;
use App\Models\Quote;
use App\Services\Quote\QuoteCreateService;
use App\Services\Quote\QuoteService;
use App\Services\Quote\QuoteUpdateService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class QuoteController extends BaseApiController
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return QuoteResource::collection(QuoteService::make()->getListWithPagination(10));
    }

    /**
     * @param Quote $quote
     * @return QuoteResource
     */
    public function show(Quote $quote): QuoteResource
    {
        return QuoteResource::make($quote);
    }

    /**
     * @return QuoteResource
     */
    public function create(): QuoteResource
    {
        return QuoteResource::make(new Quote());
    }

    /**
     * @param QuoteCreateRequest $request
     * @return QuoteResource
     */
    public function store(QuoteCreateRequest $request): QuoteResource
    {
        return QuoteResource::make(QuoteCreateService::make()->create($request));
    }

    /**
     * @param Quote $quote
     * @param QuoteUpdateRequest $request
     * @return QuoteResource
     */
    public function update(Quote $quote, QuoteUpdateRequest $request): QuoteResource
    {
        return QuoteResource::make(QuoteUpdateService::make($quote)->update($request));
    }

    /**
     * @param Quote $quote
     * @return QuoteResource
     */
    public function edit(Quote $quote): QuoteResource
    {
        return QuoteResource::make($quote);
    }

    /**
     * @param Quote $quote
     * @return JsonResource
     */
    public function destroy(Quote $quote): JsonResource
    {
        $quote->delete();

        return $this->responseSuccess();
    }
}
