<?php

namespace App\Http\Controllers;

use App\Http\Requests\Quote\QuoteCreateRequest;
use App\Http\Requests\Quote\QuoteUpdateRequest;
use App\Models\Quote;
use App\Services\Quote\QuoteCreateService;
use App\Services\Quote\QuoteService;
use App\Services\Quote\QuoteUpdateService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class QuoteController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        $quotes = QuoteService::make()->getListWithPagination(10);

        return view('public.quote.index', compact('quotes'));
    }

    /**
     * @param Quote $quote
     * @return Factory|View|Application
     */
    public function show(Quote $quote): Factory|View|Application
    {
        return view('public.quote.show', compact('quote'));
    }

    /**
     * @return Factory|View|Application
     */
    public function create(): Factory|View|Application
    {
        return view('public.quote.create', ['quote' => new Quote()]);
    }

    /**
     * @param QuoteCreateRequest $request
     * @return Redirector|Application|RedirectResponse
     */
    public function store(QuoteCreateRequest $request): Redirector|Application|RedirectResponse
    {
        $quote = QuoteCreateService::make()->create($request);

        return redirect(route('quote.edit', $quote->id))->with('success', 'Create success');
    }

    /**
     * @param Quote $quote
     * @param QuoteUpdateRequest $request
     * @return Redirector|Application|RedirectResponse
     */
    public function update(Quote $quote, QuoteUpdateRequest $request): Redirector|Application|RedirectResponse
    {
        QuoteUpdateService::make($quote)->update($request);

        return redirect(route('quote.edit', $quote->id))->with('success', 'Update success');
    }

    /**
     * @param Quote $quote
     * @return Factory|View|Application
     */
    public function edit(Quote $quote): Factory|View|Application
    {
        return view('public.quote.edit', compact('quote'));
    }

    /**
     * @param Quote $quote
     * @return Application|RedirectResponse|Redirector
     */
    public function destroy(Quote $quote): Redirector|RedirectResponse|Application
    {
        $quote->delete();

        return redirect(route('quote.index'))->with('success', 'Remove success');
    }
}
