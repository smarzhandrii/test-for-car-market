<?php

namespace App\Http\Controllers;

use App\Http\Requests\Quote\Sharing\QuoteSharingEmailRequest;
use App\Http\Requests\Quote\Sharing\QuoteSharingTelegramRequest;
use App\Http\Requests\Quote\Sharing\QuoteSharingViberRequest;
use App\Jobs\Quote\Sharing\EmailSharingJob;
use App\Jobs\Quote\Sharing\TelegramSharingJob;
use App\Jobs\Quote\Sharing\ViberSharingJob;
use App\Models\Quote;
use Illuminate\Http\RedirectResponse;

class QuoteSharingController extends Controller
{
    /**
     * @param Quote $quote
     * @param QuoteSharingTelegramRequest $request
     * @return RedirectResponse
     */
    public function sendToTelegram(Quote $quote, QuoteSharingTelegramRequest $request): RedirectResponse
    {
        $this->dispatch(new TelegramSharingJob($request->validated()['telegram'], $quote));

        return back()->with('success', 'Quote was sending');
    }

    /**
     * @param Quote $quote
     * @param QuoteSharingEmailRequest $request
     * @return RedirectResponse
     */
    public function sendToEmail(Quote $quote, QuoteSharingEmailRequest $request): RedirectResponse
    {
        $this->dispatch(new EmailSharingJob($request->validated()['email'], $quote));

        return back()->with('success', 'Quote was sending');
    }

    /**
     * @param Quote $quote
     * @param QuoteSharingViberRequest $request
     * @return RedirectResponse
     */
    public function sendToViber(Quote $quote, QuoteSharingViberRequest $request): RedirectResponse
    {
        $this->dispatch(new ViberSharingJob($request->validated()['phone'], $quote));

        return back()->with('success', 'Quote was sending');
    }
}
