<?php

namespace App\Services\ExampleCrud;

use App\Services\Traits\MakeTrait;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class IndexService
{
    use MakeTrait;

    protected string $instance;

    /**
     * For example $instance = Author::class;
     * If you use this structure need to add some checks
     *
     * @param string $instance
     */
    public function __construct(string $instance)
    {
        $this->instance = $instance;
    }

    /**
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function getListWithPagination(int $perPage): LengthAwarePaginator
    {
        return DB::table( (new $this->instance)->getTable() )->paginate( $perPage );
    }
}
