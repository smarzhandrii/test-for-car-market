<?php

namespace App\Services\Author;

use App\Http\Requests\Author\AuthorUpdateRequest;
use App\Models\Author;
use App\Services\Traits\MakeTrait;

class AuthorUpdateService
{
    use MakeTrait;

    protected Author $author;

    public function __construct(Author $author)
    {
        $this->author = $author;
    }

    /**
     * @param AuthorUpdateRequest $request
     * @return Author
     */
    public function update(AuthorUpdateRequest $request) : Author
    {
        $validatedData = $request->validated();

        return tap($this->author, function (Author $author) use ($validatedData){
            $author->username = $validatedData['username'];

            $author->save();
        });
    }
}
