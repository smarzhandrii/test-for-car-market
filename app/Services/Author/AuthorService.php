<?php

namespace App\Services\Author;

use App\Models\Author;
use App\Services\Traits\MakeTrait;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class AuthorService
{
    use MakeTrait;

    /**
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function getListWithPagination(int $perPage): LengthAwarePaginator
    {
        return Author::paginate( $perPage );
    }
}
