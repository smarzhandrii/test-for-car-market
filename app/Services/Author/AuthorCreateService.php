<?php

namespace App\Services\Author;

use App\Http\Requests\Author\AuthorCreateRequest;
use App\Models\Author;
use App\Services\Traits\MakeTrait;

class AuthorCreateService
{
    use MakeTrait;

    /**
     * @param AuthorCreateRequest $request
     * @return Author
     */
    public function create(AuthorCreateRequest $request) : Author
    {
        return tap(new Author(), function (Author $author) use ($request) {
            $validatedData = $request->validated();

            $author->email = $validatedData['email'];
            $author->username = $validatedData['username'];

            $author->save();
        });
    }

    /**
     * @param string $email
     * @param string $username
     * @return Author
     */
    public function createForQuote(string $email, string $username) : Author
    {
        if( !$author = Author::where('email', $email)->first() )
        {
            $author = tap(new Author(), function (Author $author) use ($username, $email) {
                $author->email = $email;
                $author->username = $username;

                $author->save();
            });
        }

        return $author;
    }
}
