<?php

namespace App\Services\Quote;

use App\Http\Requests\Quote\QuoteCreateRequest;
use App\Models\Quote;
use App\Services\Author\AuthorCreateService;
use App\Services\Traits\MakeTrait;

class QuoteCreateService
{
    use MakeTrait;

    /**
     * @param QuoteCreateRequest $request
     * @return Quote
     */
    public function create(QuoteCreateRequest $request): Quote
    {
        $validatedData = $request->validated();

        $author = AuthorCreateService::make()->createForQuote($validatedData['email'], $validatedData['username']);

        return tap(new Quote(), function (Quote $quote) use ($validatedData, $author) {
            $quote->author_id = $author->id;
            $quote->content = $validatedData['content'];

            $quote->save();
        });
    }
}
