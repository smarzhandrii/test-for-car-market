<?php

namespace App\Services\Quote;

use App\Http\Requests\Quote\QuoteUpdateRequest;
use App\Models\Quote;
use App\Services\Traits\MakeTrait;

class QuoteUpdateService
{
    use MakeTrait;

    protected Quote $quote;

    public function __construct(Quote $quote)
    {
        $this->quote = $quote;
    }

    /**
     * @param QuoteUpdateRequest $request
     * @return Quote
     */
    public function update(QuoteUpdateRequest $request) : Quote
    {
        $validatedData = $request->validated();

        return tap($this->quote, function (Quote $quote) use ($validatedData){
            $quote->content = $validatedData['content'];

            $quote->save();
        });
    }
}
