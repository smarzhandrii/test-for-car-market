<?php

namespace App\Services\Quote;

use App\Models\Quote;
use App\Services\Traits\MakeTrait;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class QuoteService
{
    use MakeTrait;

    /**
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function getListWithPagination(int $perPage): LengthAwarePaginator
    {
        return Quote::paginate($perPage);
    }
}
