<?php

use App\Http\Controllers\AuthorController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\QuoteController;
use App\Http\Controllers\QuoteSharingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::resource('quote', QuoteController::class);
Route::post('quote/{quote}/sharing/telegram', [QuoteSharingController::class, 'sendToTelegram'])->name('quote.sharing.telegram');
Route::post('quote/{quote}/sharing/email', [QuoteSharingController::class, 'sendToEmail'])->name('quote.sharing.email');
Route::post('quote/{quote}/sharing/viber', [QuoteSharingController::class, 'sendToViber'])->name('quote.sharing.viber');

Route::resource('author', AuthorController::class);
