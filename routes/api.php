<?php

use App\Http\Controllers\Api\Author\AuthorController;
use App\Http\Controllers\Api\Quote\QuoteController;
use App\Http\Controllers\Api\Quote\QuoteSharingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('quote', QuoteController::class);
Route::post('quote/{quote}/sharing/telegram', [QuoteSharingController::class, 'sendToTelegram'])->name('quote.sharing.telegram');
Route::post('quote/{quote}/sharing/email', [QuoteSharingController::class, 'sendToEmail'])->name('quote.sharing.email');
Route::post('quote/{quote}/sharing/viber', [QuoteSharingController::class, 'sendToViber'])->name('quote.sharing.viber');

Route::resource('author', AuthorController::class);
